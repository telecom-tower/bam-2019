module "https://gitlab.com/telecom-tower/bam2019"

go 1.12

require (
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.4.0 // indirect
	gitlab.com/telecom-tower/sdk v1.3.1
	golang.org/x/image v0.0.0-20190823064033-3a9bac650e44
	google.golang.org/grpc v1.23.0
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
